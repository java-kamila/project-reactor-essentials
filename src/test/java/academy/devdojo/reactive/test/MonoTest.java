package academy.devdojo.reactive.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.blockhound.BlockHound;
import reactor.blockhound.BlockingOperationError;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

@Slf4j
/**
 * Reactive Streams
 * 1. Asynchronous
 * 2. Non-blocking
 * 3. Backpressure
 * Publisher <- (subscribe) Subscriber
 * Subscription is created
 * Publisher (onSubscribe send the subscription) -> Subscriber
 * Subscription <- (request N) Subscriber
 * Publisher -> (onNext) Subscriber
 * until:
 * 1. Publisher sends all the objects requested.
 * 2. Publisher sends all the objects it has. (onComplete) subscriber and subscription will be canceled
 * 3. There is an error. (onError) -> subscriber and subscription will be canceled
 */
public class MonoTest {


    @BeforeAll
    public static void setUp() {
        BlockHound.install();
    }

    // Bloqueia Thread. Com BlockHound instalado, deve resultar exceção e passar no teste
    // Sem BlockHound deve falhar no teste
    @Test
    public void blockHoundWorks() {
        try {
            FutureTask<?> task = new FutureTask<>(() -> {
                Thread.sleep(0);
                return "";
            });
            Schedulers.parallel().schedule(task);

            task.get(10, TimeUnit.SECONDS);
            Assertions.fail("should fail");
        } catch (Exception e) {
            Assertions.assertTrue(e.getCause() instanceof BlockingOperationError);
        }
    }

    @Test
    public void monoSubscriber() {
        log.info("--> METHOD monoSubscriber");
        String name = "Luke Skywalker";

        // Mono é o publisher
        Mono<String> mono = Mono.just(name)
                .log(); // Observe all Reactive Streams signals and trace (onSubscribe, request, onNext, onComplete)

        mono.subscribe();
        log.info("---------------------------");

        // Executando teste, realiza subscrib e verifica o dado esperado
        StepVerifier.create(mono)
                .expectNext("Luke Skywalker")
                .verifyComplete();
    }

    @Test
    public void monoSubscriberConsumer() {
        log.info("--> METHOD monoSubscriberConsumer");
        String name = "Luke Skywalker";

        Mono<String> mono = Mono.just(name).log();

        mono.subscribe((s) -> log.info("Value: {}", s));
        log.info("---------------------------");

        // Executando teste, realiza subscrib e verifica o dado esperado
        StepVerifier.create(mono)
                .expectNext("Luke Skywalker")
                .verifyComplete();
    }

    @Test
    public void monoSubscriberConsumerError() {
        log.info("--> METHOD monoSubscriberConsumerError");
        String name = "Luke Skywalker";

        // Mono é o publisher
        Mono<String> mono = Mono.just(name).map(s -> {
            throw new RuntimeException("Teste mono with error");
        });

        mono.subscribe((s) -> log.info("Name: {}", s), s -> log.error("Something bad happend"));
        mono.subscribe((s) -> log.info("Name: {}", s), Throwable::printStackTrace);

        log.info("---------------------------");

        // Executando teste, realiza subscrib e verifica o dado esperado
        StepVerifier.create(mono)
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void monoSubscriberConsumerComplete() {
        log.info("--> METHOD monoSubscriberConsumerComplete");
        String name = "Leia Skywalker";

        Mono<String> mono = Mono.just(name)
                .log()
                .map(String::toUpperCase);

        mono.subscribe((s) -> log.info("Value: {}", s),
                Throwable::printStackTrace,
                () -> log.info("FINISHED!"));

        log.info("---------------------------");

        // Executando teste, realiza subscrib e verifica o dado esperado
        StepVerifier.create(mono)
                .expectNext(name.toUpperCase())
                .verifyComplete();
    }

    @Test
    public void monoSubscriberConsumerSubscription() {
        log.info("--> METHOD monoSubscriberConsumerSubscription");
        String name = "Obi-wan Kenobi";

        Mono<String> mono = Mono.just(name)
                .log()
                .map(String::toUpperCase);

        mono.subscribe((s) -> log.info("Value: {}", s),
                Throwable::printStackTrace,
                () -> log.info("FINISHED!"),
                subscription -> subscription.request(5)); // Cancela a inscrição e finaliza qlqr relacionamento entre publish e subscriber

        log.info("---------------------------");

        // Teste para publisher
//		StepVerifier.create(mono)
//			.expectNext(name.toUpperCase())
//			.verifyComplete();
    }

    @Test
    public void monoDoOnMethods() {
        log.info("--> METHOD monoDoOnMethods");
        String name = "Goku";

        Mono<Object> mono = Mono.just(name)
                .log()
                .map(String::toUpperCase)
                .doOnSubscribe(subscription -> log.info("doOnSubscribe"))
                .doOnRequest(longNumber -> log.info("doOnRequest. Request Received, starting doing something..."))
                .doOnNext(s -> log.info("doOnNext. Value is here:  {}", s)) // Caso não há nada pra ser emitido não será executado
                .flatMap(s -> Mono.empty())
                .doOnNext(s -> log.info("doOnNext. Value is here:  {}", s)) // Não será executado pois foram removidos os valores no Mono
                .doOnSuccess(s -> log.info("doOnSuccess executed: {}", s)); // Executado semre q o publisher emitir algum dado com sucesso. Valor nulo devido a  Mono.empty()

        mono.subscribe((s) -> log.info("Value: {}", s),
                Throwable::printStackTrace,
                () -> log.info("FINISHED!"));

    }

    @Test
    public void monoDoOnError() {
        log.info("--> METHOD monoDoOnError");

        Mono<Object> error = Mono.error(new IllegalArgumentException("Illegal argument exception"))
                .doOnError(e -> log.error("Error message: {}", e.getMessage()))
                .doOnNext(s -> log.info("Não é executado, pois o erro acontece anteriormente finalizando a subscrição"))
                .log();

        StepVerifier.create(error)
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void monoDoOnErrorResume() {
        log.info("--> METHOD monoDoOnErrorResume");
        String name = "Bulma";

        Mono<Object> error = Mono.error(new IllegalArgumentException("Illegal argument exception"))
                .doOnError(e -> log.error("Error message: {}", e.getMessage()))
                .onErrorResume(s -> { // Retorna Mono Object
                    log.info("onErrorResumo, chamado após Mono.error, fallback em caso de erro");
                    return Mono.just(name);
                })
                .doOnError(e -> log.error("Não é executado pois no novo objeto mono em onErrorResume não ocorre erro"))
                .log();

        StepVerifier.create(error)
                .expectNext(name)
                .verifyComplete();

    }

    @Test
    public void monoDoOnErrorReturn() {
        log.info("--> METHOD monoDoOnErrorResume");
        String name = "Bulma";

        Mono<Object> error = Mono.error(new IllegalArgumentException("Illegal argument exception"))
                .doOnError(e -> log.error("Error message: {}", e.getMessage()))
                .onErrorReturn("EMPTY") // Retorna Object, faz com que as funções callback abaixo sejam ignoradas
                .onErrorResume(s -> {
                    log.info("onErrorResumo, chamado após Mono.error, fallback em caso de erro, não será executado");
                    return Mono.just(name);
                })
                .doOnError(e -> log.error("Não é executado pois no novo objeto mono em onErrorResume não ocorre erro"))
                .log();

        StepVerifier.create(error)
                .expectNext("EMPTY")
                .verifyComplete();

    }

}
