package academy.devdojo.reactive.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.blockhound.BlockHound;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class OperatorTest {


    @BeforeAll
    public static void setUp() {
        BlockHound.install(builder ->
                // Informando que este metodo é bloqueando, abrindo exceção na verificação de Threads não-bloqueantes
                builder.allowBlockingCallsInside("org.slf4j.impl.SimpleLogger", "write"));
    }

    /**
     * Publish e subscribe trabalhando em threads diferentes.
     * SubscribeOn afeta tudo que foi publicado
     */
    @Test
    public void subscribeOnSimple() {
        Flux<Integer> flux = Flux.range(1, 4)
                .map(i -> {
                    log.info("Map 1 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                })
                // Cria thread em background 'boundedElastic-1' tudo publicado é executado nela
                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("Map 2 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(flux)
                .expectSubscription()
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }

    /**
     * PublishOn afeta o que for publicado após sua declaração
     */
    @Test
    public void publishOnSimple() {
        Flux<Integer> flux = Flux.range(1, 4)
                .map(i -> {
                    log.info("Map 1 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                })
                // Os objetos já publicados foram executados na thread main
                // publishOn cria thread 'boundedElastic-X' e os eventos publicados em seguida
                // serão processados nela
                .publishOn(Schedulers.boundedElastic()) //
                .map(i -> {
                    log.info("Map 2 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                });

        flux.subscribe();
        flux.subscribe();

//        StepVerifier.create(flux)
//                .expectSubscription()
//                .expectNext(1, 2, 3, 4)
//                .verifyComplete();
    }

    /**
     * Utilizando mais de um subscribeOn, recomendado não replicar, método apenas para compreensão
     */
    @Test
    public void multipleSubscribeOnSimple() {
        Flux<Integer> flux = Flux.range(1, 4)
                .subscribeOn(Schedulers.single()) // Thread q será utilizada em todo o fluxo
                .map(i -> {
                    log.info("Map 1 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                })
                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("Map 2 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(flux)
                .expectSubscription()
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }

    @Test
    public void multiplePublishOnSimple() {
        Flux<Integer> flux = Flux.range(1, 4)
                // Map 1 é executado no Thread single-1
                .publishOn(Schedulers.single())
                .map(i -> {
                    log.info("Map 1 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                })
                // Map 1 é executado no Thread boundedElastic-1
                .publishOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("Map 2 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(flux)
                .expectSubscription()
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }

    @Test
    public void publishOnAndSubscribeOnSimple() {
        Flux<Integer> flux = Flux.range(1, 4)
                // Todas as subscrições ocorem Thread single-1, publishOn tem prioridade
                .publishOn(Schedulers.single())
                .map(i -> {
                    log.info("Map 1 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                })
                // subscribeOn é ignorado
                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("Map 2 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(flux)
                .expectSubscription()
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }

    @Test
    public void subscribeOnAndPublishOnSimple() {
        Flux<Integer> flux = Flux.range(1, 4)
                // Map 1 executado na Thread single-1
                .subscribeOn(Schedulers.single())
                .map(i -> {
                    log.info("Map 1 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                })
                // Map 2 executado na Thread boundedElastic-1, publishOn afeta o que vem depois
                .publishOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("Map 2 - Number {} on Thread {}", i, Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(flux)
                .expectSubscription()
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }

    @Test
    public void subscribeOnIO() {
        // fromCallable, cria Mono a partir de execução, poderia sr chamada para API externa por exemplo
        Mono<List<String>> list = Mono.fromCallable(() -> Files.readAllLines(Path.of("text-file")))
                .log()
                // Executando thread em background
                .subscribeOn(Schedulers.boundedElastic());

        list.subscribe(s -> log.info("{}", s));

        StepVerifier.create(list)
                .expectSubscription()
                .thenConsumeWhile(l -> {
                    Assertions.assertFalse(l.isEmpty());
                    log.info("Size {}", l.size());
                    return true;
                }).verifyComplete();
    }

    /**
     * switchIfEmpty: Subscribe em publisher que retorna vazio
     * é possível setar um valor default para o retorno.
     */
    @Test
    public void switchIfEmptyOperator() {
        // Forma declarativa de verificar publisher empty e retornar um dado
        Flux<Object> flux = emptyFlux()
                .switchIfEmpty(Flux.just("Not empty anymore."))
                .log();

        StepVerifier.create(flux)
                .expectSubscription()
                .expectNext("Not empty anymore.")
                .expectComplete()
                .verify();
    }

    /**
     * Adia a execução do operador
     */
    @Test
    public void deferOperator() throws Exception {
        // Mono é capturado no momento de instanciação
        Mono<Long> just = Mono.just(System.currentTimeMillis());
        // Todos os logs abaixo imprimirão o mesmo instante, da instanciação acima
        just.subscribe(l -> log.info("Just time {}", l));
        Thread.sleep(100);
        just.subscribe(l -> log.info("Just time {}", l));
        Thread.sleep(100);
        just.subscribe(l -> log.info("Just time {}", l));
        Thread.sleep(100);
        just.subscribe(l -> log.info("Just time {}", l));

        Mono<Long> defer = Mono.defer(() -> Mono.just(System.currentTimeMillis()));
        // Aplica o supplier para cada subscrição. Cada subscrição abaixo imprimirá instantes diferentes
        defer.subscribe(l -> log.info("Defer time {}", l));
        Thread.sleep(100);
        defer.subscribe(l -> log.info("Defer time {}", l));
        Thread.sleep(100);
        defer.subscribe(l -> log.info("Defer time {}", l));
        Thread.sleep(100);
        defer.subscribe(l -> log.info("Defer time {}", l));

        AtomicLong atomicLong = new AtomicLong();
        defer.subscribe(atomicLong::set);
        Assertions.assertTrue(atomicLong.get() > 0);
    }

    @Test
    public void concatOperator() {
        Flux<String> flux1 = Flux.just("a", "b");
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> concatFlux = Flux.concat(flux1, flux2).log();

        StepVerifier.create(concatFlux)
                .expectSubscription()
                .expectNext("a", "b", "c", "d")
                .expectComplete()
                .verify();
    }

    /**
     * concat, ao ocorrer uma exceção a concatenação é interrompida
     */
    @Test
    public void concatOperatorError() {
        Flux<String> flux1 = Flux.just("a", "b")
                .map(s -> { // Aplica os valores de forma síncrona em cada um dos elementos do Flux
                    if (s.equals("b")) {
                        throw new IllegalArgumentException();
                    }
                    return s;
                });
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> concatFlux = Flux.concat(flux1, flux2).log();

        StepVerifier.create(concatFlux)
                .expectSubscription()
                .expectNext("a") // Apenas o "a" é pubicado, pois no "b" há uma exception e o fluxo é interrompido
                .expectError()
                .verify();
    }

    /**
     * concat, ao ocorrer uma exceção a concatenação continua, os dados publicados após serão concatenados.
     * Adia o erro.
     */
    @Test
    public void concatDelayOperatorError() {
        Flux<String> flux1 = Flux.just("a", "b")
                .map(s -> { // Aplica os valores de forma síncrona em cada um dos elementos do Flux
                    if (s.equals("b")) {
                        throw new IllegalArgumentException();
                    }
                    return s;
                });
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> concatFlux = Flux.concatDelayError(flux1, flux2).log();

        StepVerifier.create(concatFlux)
                .expectSubscription()
                .expectNext("a", "c", "d") // Apenas o "b" é perdido pois ocorre exceção na sua publicação
                .expectError()
                .verify();
    }

    @Test
    public void concatWithOperator() {
        Flux<String> flux1 = Flux.just("a", "b");
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> concatFlux = flux1.concatWith(flux2).log();

        StepVerifier.create(concatFlux)
                .expectSubscription()
                .expectNext("a", "b", "c", "d")
                .expectComplete()
                .verify();
    }

    @Test
    public void combineLastOperator() {
        Flux<String> flux1 = Flux.just("a", "b");
        Flux<String> flux2 = Flux.just("c", "d");

        // Captura o último valor publicado de um Flux com o último publicado de outro Flux
        Flux<String> combineLatest = Flux.combineLatest(flux1, flux2,
                (s1, s2) -> s1.toUpperCase() + s2.toUpperCase()
        ).log();

        // Teste não garantido, pode mudar em decorrência da velocidade de publicação
        StepVerifier.create(combineLatest)
                .expectSubscription()
                .expectNext("BC", "BD")
                .expectComplete()
                .verify();
    }

    /**
     * Subscribe Eagerly, merge não espera que primeiro publisher finalize completamente
     */
    @Test
    public void mergeOperator() {
        Flux<String> flux1 = Flux.just("a", "b").delayElements(Duration.ofMillis(200));
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> mergeFlux = Flux.merge(flux1, flux2)
                .delayElements(Duration.ofMillis(200))
                .log();

        Flux<String> mergeTwoFlux = flux1.mergeWith(flux2)
                .delayElements(Duration.ofMillis(200))
                .log();

        mergeFlux.subscribe(log::info);
        // São capturados os elementos do flux2 pois são publicados primeiro, em seguida os elementos do flux1
        StepVerifier.create(mergeTwoFlux)
                .expectSubscription()
                .expectNext("c", "d", "a", "b")
                .expectComplete()
                .verify();
    }

    /**
     * Merge de forma sequencial, espera a execução do publisher de cada flux
     */
    @Test
    public void mergeSequentialOperator() {
        Flux<String> flux1 = Flux.just("a", "b").delayElements(Duration.ofMillis(200));
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> mergeSequential = Flux.mergeSequential(flux1, flux2, flux1)
                .delayElements(Duration.ofMillis(200))
                .log();

        StepVerifier.create(mergeSequential)
                .expectSubscription()
                .expectNext("a", "b", "c", "d", "a", "b")
                .expectComplete()
                .verify();
    }

    @Test
    public void mergeDelayErrorOperator() {
        Flux<String> flux1 = Flux.just("a", "b")
                .map(s -> { // Aplica os valores de forma síncrona em cada um dos elementos do Flux
                    if (s.equals("b")) {
                        throw new IllegalArgumentException();
                    }
                    return s;
                }).doOnError(t -> log.error("ERROR -> We could do something with this!"));
        Flux<String> flux2 = Flux.just("c", "d");

        Flux<String> mergeDelayError = Flux.mergeDelayError(1, flux1, flux2, flux1)
                .log();

        StepVerifier.create(mergeDelayError)
                .expectSubscription()
                .expectNext("a", "c", "d", "a") // perde apenas os itens publicados com erro
                .expectError()
                .verify();
    }

    private Flux<Object> emptyFlux() {
        return Flux.empty();
    }

    @Test
    public void flatMapOperator() {
        Flux<String> flux = Flux.just("a", "b");
        // Flux de fluxs, na verdade desejamos extrair o resultado do primeiro Flux, que seria o Flux<String>
        Flux<Flux<String>> fluxFlux = flux.map(String::toUpperCase)
                .map(this::findByName)
                .log();
        fluxFlux.subscribe(o -> log.info(o.toString()));

        // FlatMap, de forma assíncrona, próximo ao merge operator, extrai o Flux<String>
        Flux<String> flatFlux = flux.map(String::toUpperCase)
                .flatMap(this::findByName)
                .log();
        StepVerifier.create(flatFlux)
                .expectSubscription()
                .expectNext("nameA1", "nameA2", "nameB1", "nameB2")
                .verifyComplete();
    }

    @Test
    public void flatMapOperatorDelay() {
        Flux<String> flux = Flux.just("a", "b");
        // FlatMap, de forma assíncrona, próximo ao merge operator, extrai o Flux<String>.
        // COmo nameA1 e A2 tem um delay para responder nameB1 e B2 são capturados primeiro
        Flux<String> flatFlux = flux.map(String::toUpperCase)
                .flatMap(this::findByNameDelay)
                .log();

        StepVerifier.create(flatFlux)
                .expectSubscription()
                .expectNext("nameB1", "nameB2", "nameA1", "nameA2")
                .verifyComplete();
    }

    @Test
    public void flatMapSequentialOperator() {
        Flux<String> flux = Flux.just("a", "b");
        // FlatMap, de forma síncrona, extrai o Flux<String> na ordem de declaração
        // Ainda que B1 e B2 tenham sido publicados primeiro
        Flux<String> flatFlux = flux.map(String::toUpperCase)
                .flatMapSequential(this::findByNameDelay)
                .log();

        StepVerifier.create(flatFlux)
                .expectSubscription()
                .expectNext("nameA1", "nameA2", "nameB1", "nameB2")
                .verifyComplete();
    }

    public Flux<String> findByName(String name) {
        return name.equals("A") ? Flux.just("nameA1", "nameA2") : Flux.just("nameB1", "nameB2");
    }

    public Flux<String> findByNameDelay(String name) {
        if (name.equals("A")) {
            return Flux.just("nameA1", "nameA2")
                    .delayElements(Duration.ofMillis(200));
        } else {
            return Flux.just("nameB1", "nameB2");
        }
    }


    @AllArgsConstructor
    @Data
    @EqualsAndHashCode
    static class Anime {
        private String title;
        private String studio;
        private int episodes;
    }

    // Zip: Captura o primeiro elemento de cada publisher e aplica uma função
    @Test
    public void zipOperator() {
        Flux<String> titleFlux = Flux.just("Grand Blue", "Baki");
        Flux<String> studioFlux = Flux.just("Zero-G", "IMS Entertainment");
        Flux<Integer> episodesFlux = Flux.just(12, 24);

        // Criando objeto do tipo Anime com os valores correspondentes em ordem da cada Flux
        // Tuple, estrutura que guarda n elementos
        // Flux<Tuple3<String, String, Integer>> zip = Flux.zip(titleFlux, studioFlux, episodesFlux);

        Flux<Anime> animeFlux = Flux.zip(titleFlux, studioFlux, episodesFlux)
                .flatMap(tuple ->
                        Flux.just(new Anime(tuple.getT1(), tuple.getT2(), tuple.getT3()))
                );

        animeFlux.subscribe(anime -> log.info(anime.toString()));

        StepVerifier.create(animeFlux)
                .expectSubscription()
                .expectNext(
                        new Anime("Grand Blue", "Zero-G", 12),
                        new Anime("Baki", "IMS Entertainment", 24))
                .verifyComplete();
    }

    // ZipWith: Captura o primeiro elemento de dois publishers
    @Test
    public void zipWithOperator() {
        Flux<String> titleFlux = Flux.just("Grand Blue", "Baki");
        Flux<String> studioFlux = Flux.just("Zero-G", "IMS Entertainment");
        Flux<Integer> episodesFlux = Flux.just(12, 24);

        Flux<Anime> animeFlux = titleFlux.zipWith(episodesFlux)
                .flatMap(tuple ->
                        Flux.just(new Anime(tuple.getT1(), null, tuple.getT2()))
                );

        animeFlux.subscribe(anime -> log.info(anime.toString()));

        StepVerifier.create(animeFlux)
                .expectSubscription()
                .expectNext(
                        new Anime("Grand Blue", null, 12),
                        new Anime("Baki", null, 24))
                .verifyComplete();
    }


}
