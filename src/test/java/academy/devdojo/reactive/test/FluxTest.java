package academy.devdojo.reactive.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.blockhound.BlockHound;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.List;

@Slf4j
public class FluxTest {


    @BeforeAll
    public static void setUp() {
        BlockHound.install();
    }

    @Test
    public void FluxSubscriber() {
        Flux<String> fluxString = Flux.just("Super Patos", "Hércules", "Três Espiãs Demais")
                .log();

        StepVerifier.create(fluxString)
                .expectNext("Super Patos", "Hércules", "Três Espiãs Demais")
                .verifyComplete();
    }

    @Test
    public void FluxSubscriberNumbers() {
        Flux<Integer> fluxNumber = Flux.range(1, 5)
                .log();

        // Subscribe é executado após cada número publicado no .range
        fluxNumber.subscribe(i -> log.info("Number: {}", i));

        log.info("-----------");

        StepVerifier.create(fluxNumber)
                .expectNext(1, 2, 3, 4, 5)
                .verifyComplete();
    }

    @Test
    public void FluxSubscriberFromList() {
        Flux<Integer> flux = Flux.fromIterable(List.of(1, 2, 3, 4, 5))
                .log();

        // Subscribe é executado após cada número publicado no .range
        flux.subscribe(i -> log.info("Number: {}", i));

        log.info("-----------");

        StepVerifier.create(flux)
                .expectNext(1, 2, 3, 4, 5)
                .verifyComplete();
    }

    @Test
    public void FluxSubscriberNumbersError() {
        Flux<Integer> flux = Flux.range(1, 5)
                .log()
                .map(i -> {
                    if (i == 4) {
                        throw new IndexOutOfBoundsException("Index error!");
                    }
                    return i;
                });

        flux.subscribe(i -> log.info("Number: {}", i),
                Throwable::printStackTrace,
                () -> log.info("DONE!"), // Não executado pois publisher não publica até o 5
                subscription -> subscription.request(3)); // Backpressure: Pode emitir 5 subscriptions, porém vamos ouvir apenas 3

        log.info("-----------");

        StepVerifier.create(flux)
                .expectNext(1, 2, 3)
                .expectError(IndexOutOfBoundsException.class)
                .verify();
    }

    @Test
    public void FluxSubscriberNumbersUglyBackpressure() {
        Flux<Integer> flux = Flux.range(1, 10)
                .log();
        // Requisita 2 subscrições por vez
        flux.subscribe(new Subscriber<Integer>() {
            private int count = 0;
            private Subscription subscription;
            private int requestCount = 2;

            @Override
            public void onSubscribe(Subscription subscription) {
                this.subscription = subscription;
                subscription.request(requestCount);
            }

            @Override
            public void onNext(Integer integer) {
                count++;
                if (count >= 2) {
                    count = 0;
                    subscription.request(requestCount);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onComplete() {
                // TODO Auto-generated method stub

            }
        });

        log.info("-----------");

        StepVerifier.create(flux)
                .expectNext(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .verifyComplete();
    }

    /**
     * Backpressure a cada 2 subscrições
     */
    @Test
    public void FluxSubscriberNumbersUglyBackpressureV2() {
        Flux<Integer> flux = Flux.range(1, 10)
                .log();
        // Requisita 2 subscrições por vez
        flux.subscribe(new BaseSubscriber<Integer>() {
            private int count = 0;
            private final int requestCount = 2;

            @Override
            protected void hookOnSubscribe(Subscription subscription) {
                request(2);
            }

            @Override
            protected void hookOnNext(Integer value) {
                count++;
                if (count >= 2) {
                    count = 0;
                    request(requestCount);
                }
            }
        });

        log.info("-----------");

        StepVerifier.create(flux)
                .expectNext(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .verifyComplete();
    }

    /**
     * Backpressure a cada 3 subscrições
     */
    @Test
    public void FluxSubscriberPrettyBackpressure() {
        Flux<Integer> fluxNumber = Flux.range(1, 10)
                .log()
                .limitRate(3);  // Requisita 3 subscrições por vez

        // Subscribe é executado após cada número publicado no .range
        fluxNumber.subscribe(i -> log.info("Number: {}", i));

        log.info("-----------");

        StepVerifier.create(fluxNumber)
                .expectNext(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .verifyComplete();
    }

    /**
     * Executa a subscrição a cada intervalo de tempo
     */
    @Test
    public void fluxSubscriberIntervalOne() throws Exception {
        Flux<Long> interval = Flux.interval(Duration.ofMillis(100))
                .take(10) // limitando até o limite de 10 subscrições
                .log();

        interval.subscribe(i -> log.info("Number {}", i));

        // Parando a thread principal, necessário para visualizar logs
        Thread.sleep(3000);
    }

    /* Publisher tem q ser criado dentro do intervalTime (Reactor)
     * interval é publicado em Thread paralela ao main
     */
    @Test
    public void fluxSubscriberIntervalTwo() {
        StepVerifier.withVirtualTime(this::createInterval)
                .expectSubscription()
                .expectNoEvent(Duration.ofDays(1)) // Espera q nada tenha sido publicado no primeiro dia
                .thenAwait(Duration.ofDays(1)) // Vamos esperar por este método por x dias
                .expectNext(0L) // Espero que o valor 0L seja publicado na primeira publicação
                .thenAwait(Duration.ofDays(1))
                .expectNext(1L) // Espero que o valor 1L seja publicado na próxima publicação
                .thenCancel()
                .verify();
    }

    public Flux<Long> createInterval() {
        return Flux.interval(Duration.ofDays(1))
                .take(10) // limitando até o limite de 10 subscrições
                .log();
    }


    // Hot Publisher é executado no momento em que é conectado,
    // independente de ter-se subscrevido ou não.
    @Test
    public void connectableFlux() throws Exception {
        ConnectableFlux<Integer> connectableFlux = Flux.range(1, 10)
//                .log()
                .delayElements(Duration.ofMillis(100))
                .publish();
        // Conectado na main thread
        connectableFlux.connect();

        log.info("Thread sleeping for 300ms");
        Thread.sleep(300);
        // Perde os 3 primeiros números pois ao se sobscrever eles já haviam sido publicados
        connectableFlux.subscribe(i -> log.info("Sub1 number {}", i));

        log.info("Thread sleeping for 200ms");
        Thread.sleep(200);
        // Perde os 5 primeiros números pois ao se sobscrever eles já haviam sido publicados
        connectableFlux.subscribe(i -> log.info("Sub2 number {}", i));

        StepVerifier.create(connectableFlux)
                .then(connectableFlux::connect)
                .thenConsumeWhile(i -> i <= 5)
                .expectNext(6, 7, 8, 9, 10)
                .expectComplete()
                .verify();
    }

    @Test
    public void connectableFluxAutoConnect() throws Exception {
        // Define quantidade mínima de subscribers para começar a publicar os eventos
        Flux<Integer> fluxAutoConnect = Flux.range(1, 5)
                .log()
                .delayElements(Duration.ofMillis(100))
                .publish()
                .autoConnect(2);

        StepVerifier
                .create(fluxAutoConnect)
                .then(fluxAutoConnect::subscribe) // Segundo subscriber
                .expectNext(1, 2, 3, 4, 5)
                .expectComplete()
                .verify();
    }

}
