# Project Reactor Essentials 

Projeto de estudo com DevDojo Academy sobre fundamentos de Reactive Programming ([playlist](https://www.youtube.com/watch?v=j3Ik6iscbu8&list=PL62G310vn6nG3sBMCIEoZBK3r3E_4aKW5)).<br>
Código em classes de teste.

Programação reativa é um conceito. Baseia-se em eventos e data streams, trabalhando de forma assíncrona e não bloqueante.
Backpressure, Functional/Declarative Programming.

 - Cold: os publicadores de eventos são "preguiçosos" (lazy), só publicam se houver ouvintes inscritos.
 - Hot: eventos são publicados mesmo que não haja ouvintes.

### Reactive Streams:
 - Processar quantidade infinita de elementos
 - Processar elementos assincronamente entre componentes
 - Possuir estrutura não bloqueante com suporte a backpressure (contrapressão, gerenciamento de demanda dos publicadores).
   
 > A contrapressão é quando um downstream pode dizer a um upstream para enviar menos dados para evitar que seja sobrecarregado.
   
 - API componentes (interfaces):
    - Publisher, publica um evento
    - Subscriber, consume o evento que foi publicado
    - Subscription, contrato entre publisher e subscriber
    - Processor, estado de processamento de dados.

Implementação escolhida: [`Project Reactor`](https://projectreactor.io). <br>
Subscriber é o que gerencia o relacionamento de subscrição. <br>
No project reactor se não for instruído a executar concorrência executa processamento na main thread.

### Mono
Retorna um ou nenhum elemento.
Exemplo de subscrição no Mono com consumer, error e complete:

```java
   Mono<String> mono = Mono.just("Teste");
   
   mono.subscribe((s) -> log.info("Value: {}", s),
          Throwable::printStackTrace,
          () -> log.info("Complete: FINISHED!"));
```

### Flux
Retorna um ou vários elementos.
Intervalos, realiza um agendamento e publica algo durante esse agendamento.


### Contrapressão
Exemplo de implementação de contrapressão, informando ao upstream para enviar apenas dois elementos por vez:

```java
Flux.just(1, 2, 3, 4)
  .log()
  .subscribe(new Subscriber<Integer>() {
    private Subscription s;
    int onNextAmount;

    @Override
    public void onSubscribe(Subscription s) {
        this.s = s;
        s.request(2);
    }

    @Override
    public void onNext(Integer integer) {
        elements.add(integer);
        onNextAmount++;
        if (onNextAmount % 2 == 0) {
            s.request(2);
        }
    }

    @Override
    public void onError(Throwable t) {}

    @Override
    public void onComplete() {}
});
```

Saída:
```
23:31:15.395 [main] INFO  reactor.Flux.Array.1 - | onSubscribe([Synchronous Fuseable] FluxArray.ArraySubscription)
23:31:15.397 [main] INFO  reactor.Flux.Array.1 - | request(2)
23:31:15.397 [main] INFO  reactor.Flux.Array.1 - | onNext(1)
23:31:15.398 [main] INFO  reactor.Flux.Array.1 - | onNext(2)
23:31:15.398 [main] INFO  reactor.Flux.Array.1 - | request(2)
23:31:15.398 [main] INFO  reactor.Flux.Array.1 - | onNext(3)
23:31:15.398 [main] INFO  reactor.Flux.Array.1 - | onNext(4)
23:31:15.398 [main] INFO  reactor.Flux.Array.1 - | request(2)
23:31:15.398 [main] INFO  reactor.Flux.Array.1 - | onComplete()
```

Forma mais resumida:
```java
    @Test
    public void FluxSubscriberPrettyBackpressure() {
        Flux<Integer> fluxNumber = Flux.range(1, 10)
                .log()
                .limitRate(3);  // Requisita 3 subscrições por vez
        fluxNumber.subscribe(i -> log.info("Number: {}", i));
    }
```

### Leituras

 - [Artigo](https://www.baeldung.com/reactor-combine-streams) sobre combinação de streams, exemplificando métodos estáticos na classe Flux<T> como concat(), concatWith(), combineLatest(), merge(), mergeSequential(), mergeDelayError(), mergeWith(), zip(), zipWith(). 

 - [ConnectableFlux](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/ConnectableFlux.html): A classe base abstrata que permite que os assinantes se acumulem antes de se conectarem à sua fonte de dados.
 
 - autoConnect(int minSubscribers): Conecta o ConnectableFlux à fonte upstream quando a quantidade especificada de assinantes se inscreve.

 - [BlockHound](https://github.com/reactor/BlockHound/blob/master/docs/tips.md): Agente Java para detectar chamadas de bloqueio de threads não bloqueantes.